#!/usr/bin/wish

set rucksackList [list]

proc listFromFile {filename} {
    set f [open $filename r]
    set data [split [string trim [read $f]]]
    close $f
    return $data
}

proc findDuplicates {} {
    global rucksackList
    set duplicatedItem 0
    set i 0
    set itemList [list]
    foreach n $rucksackList {
	set rucksack($i) $n
	incr i
    }
    for {set i 0} {$i < [array size rucksack]} {incr i} {
 	set half [expr [string length $rucksack($i)] / 2]
	for {set j 0} {$j < $half} {incr j} {
	    for {set k 0} {$k < $half} {incr k} {
		set item [string index $rucksack($i) $j]
		if {[string index $rucksack($i) [expr ($k+$half)]] == $item} {
		    if {[scan $item %c] > 91} {
			set itemList [lreplace $itemList $i $i [expr ([scan $item %c] - 96)]]
		    } else {
			set itemList [lreplace $itemList $i $i [expr ([scan $item %c] - 38)]]
		    }
		    break
 		}
	    }
	}
    }
    puts "Duplicates are: $itemList"
    set suma 0
    set i 0
    foreach char $itemList {
	set suma [expr ($suma + $char)]
	incr i
    }
    puts "Task 1: Sum of priorities is $suma"
}


### main ###

set rucksackList [listFromFile filelist.txt]
findDuplicates

## 03.tcl ends here ##
